base:

  '*':
    - base

  'db*':
    - base.mysql
    - mysql.db
{% if 'cloud.ecg.so' in salt['grains.get']('fqdn') %}
    - base.consul
    - consul.client

  'bastion*':
    - openvpn

  'consul*':
    - base.consul
    - consul.server

  'fe*':
    - base.consul
    - consul.client

{% endif %}
