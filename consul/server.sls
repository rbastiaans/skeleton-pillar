consul:
  cluster:
    name: general

  auto_bootstrap: false
  resolv_consul: true

  config:
    encrypt: <encryption-key> #CHANGEME
    enable_debug: true
    datacenter: ams1
    bootstrap_expect: 3
    retry_interval: 15s
    client_addr: 0.0.0.0
    server: true
    ui_dir: /usr/share/consul-ui
