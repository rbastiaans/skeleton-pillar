consul:
  cluster:
    name: general

  auto_bootstrap: true
  resolv_consul: true

  config:
    encrypt: <encryption-key> #CHANGEME
    enable_debug: true
    datacenter: <dc> #CHANGEME
    retry_interval: 15s
    server: false
