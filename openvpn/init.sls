openvpn_ca_crt: |
  -----BEGIN CERTIFICATE-----
  <RAW DATA> #CHANGEME
  -----END CERTIFICATE-----

openvpn_host_key: |
  -----BEGIN PRIVATE KEY-----
  <RAW DATA> #CHANGEME
  -----END PRIVATE KEY-----

openvpn_host_crt: |
  -----BEGIN CERTIFICATE-----
  <RAW DATA> #CHANGEME
  -----END CERTIFICATE-----

openvpn_dh_params: |
  -----BEGIN DH PARAMETERS-----
  <RAW DATA> #CHANGEME
  -----END DH PARAMETERS-----

openvpn_client:
  vip_address: <external-ip-for-vpn> # CHANGEME
  vip_port: <external-port-for-vpn> # CHANGEME

openvpn_users:
  <user>: |
    <google-authenticator-data> # CHANGEME
