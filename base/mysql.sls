mysql:
    # Defaults overrides
    lookup:
        debconf_pkg_name: percona-server-server
        server: percona-server-server-5.6
        client: percona-server-client-5.6

    server:
        # my.cnf configuration
        mysqld:
            bind_address: 0.0.0.0
            binlog_format: STATEMENT
            datadir: /var/lib/mysql/data
            innodb_data_home_dir: /var/lib/mysql/data
            innodb_file_per_table: 'ON'
            innodb_log_group_home_dir: /var/lib/mysql/journal/
            log_bin: /var/lib/mysql/binary/mysqld-binlog
            log_bin_index: /var/lib/mysql/binary/mysqld-binlog.index
            log_error: /var/lib/mysql/log/mysqld-err.log
            master_info_repository: TABLE
            relay_log: /var/lib/mysql/relay/relay-bin
            relay_log_index: /var/lib/mysql/relay/relay-bin.index
            relay_log_info_repository: TABLE
            relay_log_recovery: 'ON'
            tmpdir: /var/lib/mysql/tmp
