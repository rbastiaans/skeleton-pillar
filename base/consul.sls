consul:
  cluster:
    name: cluster1

  version: 0.6.4-1
  auto_bootstrap: false
  resolv_consul: false

  config:
    bind_addr: 0.0.0.0
    client_addr: 127.0.0.1
    datacenter: main
    data_dir: /var/lib/consul
    disable_update_check: true
    enable_debug: false
    enable_syslog: true
    log_level: info
    retry_interval: 30s
    retry_join: []
    retry_join_wan: []
    server: false
