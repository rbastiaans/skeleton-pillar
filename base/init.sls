saltmasters:
  - saltmaster001

timezone:
  name: 'Europe/Amsterdam'
  utc: True

mine_functions:
  network.ip_addrs: [eth0]
  network.get_hostname: []

ldap_base: "ou=<org-unit>,o=<org>" # CHANGEME
ldap_servers:
  - ldap://<ldap-url> # CHANGEME
ldap_access_groups:
  - <posix-groups> # CHANGEME
ldap_sudoers_base: "ou=<sudo-org-unit>,ou=<org-unit>,o=<org>" # CHANGEME
ldap_access_users: []
ldap_ca_crt: |
  -----BEGIN CERTIFICATE-----
  <RAW CRT DATA> # CHANGEME
  -----END CERTIFICATE-----

ldap_noldap_passwd: "<pass-hash>" # CHANGEME
openvpn_ca_crt: |
  -----BEGIN CERTIFICATE-----
  <RAW CRT DATA> # CHANGEME
  -----END CERTIFICATE-----

openvpn_host_key: |
  -----BEGIN PRIVATE KEY-----
  <RAW KEY DATA> # CHANGEME
  -----END PRIVATE KEY-----

openvpn_host_crt: |
  -----BEGIN CERTIFICATE-----
  <RAW CRT DATA> # CHANGEME
  -----END CERTIFICATE-----

openvpn_dh_params: |
  -----BEGIN DH PARAMETERS-----
  <RAW CRT DATA> # CHANGEME
  -----END DH PARAMETERS-----
