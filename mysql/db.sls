mysql:
    user_nobinlog: true

    # Defaults overrides
    lookup:
        version: 5.6.30-76.3-1.trusty

    consul:
        cluster: db
        debug: NO
        run_interval: 0.1
        credential_file: /etc/mysql/mysql-consul.cnf

    # Server properties
    server:
        root_user: 'root'
        root_password: '<password>' #CHANGEME

        # my.cnf configuration
        mysqld:
            # needs to be an array/list
            binlog_ignore_db:
                - mysql
                - performance_schema
                - information_schema
                - test
            enforce_gtid_consistency: noarg_present
            expire_logs_days: 1
            gtid_mode: 'ON'
            innodb_adaptive_flushing: 1
            innodb_buffer_pool_dump_at_shutdown: 'ON'
            innodb_buffer_pool_instances: 2
            innodb_buffer_pool_load_at_startup: 'ON'
            innodb_buffer_pool_populate: 1
            innodb_buffer_pool_size: 23G
            innodb_data_file_path: ibdata1:20M:autoextend
            innodb_flush_log_at_trx_commit: 0
            innodb_flush_method: O_DIRECT
            innodb_io_capacity: 1500
            innodb_log_file_size: 512M
            innodb_max_dirty_pages_pct: 98
            innodb_open_files: 1024
            innodb_rollback_on_timeout: 'ON'
            innodb_stats_on_metadata: 0
            innodb_status_file: 1
            innodb_thread_concurrency: 20
            key_buffer_size: 200M
            log_queries_not_using_indexes: 1
            log_slave_updates: noarg_present
            log_slow_slave_statements: 1
            log_slow_verbosity: microtime,query_plan,innodb,profiling
            log_warnings: 2
            long_query_time: 0.5
            max_allowed_packet: 16M
            max_binlog_size: 1G
            max_connections: 1100
            max_heap_table_size: 256M
            max_user_connections: 1000
            query_cache_size: 0
            query_cache_type: 0
            # needs to be an array/list
            replicate_ignore_db:
                - mysql
                - performance_schema
                - information_schema
                - test
            # needs to be an array/list
            replicate_wild_ignore_table:
                - mysql.%
                - performance_schema.%
                - information_schema.%
                - test.%
            skip_external_locking: noarg_present
            skip_name_resolve: noarg_present
            slave_net_timeout: 30
            slave_parallel_workers: 10
            slow_query_log: noarg_present
            slow_query_log_file: /var/lib/mysql/log/mysqld-slow.log
            slow_query_log_timestamp_always: 1
            slow_query_log_timestamp_precision: microsecond
            sql_mode: NO_AUTO_CREATE_USER,STRICT_ALL_TABLES
            sync_binlog: 0
            table_open_cache: 1024
            table_open_cache_instances: 10
            thread_cache_size: 64
            thread_stack: 1024K
            tmp_table_size: 256M
            userstat: 1
        mysqld_safe:
            open-files-limit: 32768

    schema:
        percona:
          load: True
          source: salt://mysql/files/bootstrap/mysql-failover.sql

    #    klantspecifiek:
    #      load: True
    #      source: salt://mysql/files/bootstrap/mysql-dkvm.sql
    #    dkvm:
    #      load: True
    #      source: salt://mysql/files/bootstrap/mysql-dkvm.sql

    # User management
    user:
        consul:
            password: '<password>' #CHANGEME
            host: 'localhost'
            databases:
                - database: '*'
                  grants: ['SELECT', 'REPLICATION CLIENT', 'REPLICATION SLAVE', 'CREATE']
        collectd:
            password: '<password>' #CHANGEME
            host: 'localhost'
            databases:
                - database: '*'
                  grants: ['PROCESS', 'REPLICATION CLIENT', 'SHOW DATABASES', 'SUPER']
                - database: 'mysql'
                  grants: ['SELECT']
        nagios:
            password: '<password>' #CHANGEME
            host: '%'
            databases:
                - database: '*'
                  grants: ['PROCESS', 'REPLICATION CLIENT', 'SHOW DATABASES', 'SUPER']
                - database: 'integration_mp'
                  grants: ['SELECT']
                - database: 'integration_order'
                  grants: ['SELECT']
        replicate:
            password: '<password>' #CHANGEME
            host: '%'
            databases:
                - database: '*'
                  grants: ['REPLICATION SLAVE']
        failover:
            password: '<password>' #CHANGEME
            host: '%'
            databases:
                - database: '*'
                  grants: ['ALL']
        dkvm_ro:
            password: '<password>' #CHANGEME
            host: '%'
            databases:
                - database: 'dkvm'
                  grants: ['SELECT']
                - database: 'klantspecifiek'
                  grants: ['SELECT']
        dkvm_rw:
            password: '<password>' #CHANGEME
            host: '%'
            databases:
                - database: 'dkvm'
                  grants: ['SELECT', 'INSERT', 'UPDATE', 'DELETE', 'CREATE', 'ALTER', 'DROP', 'LOCK TABLES', 'INDEX']
                - database: 'klantspecifiek'
                  grants: ['SELECT', 'INSERT', 'UPDATE', 'DELETE', 'CREATE', 'ALTER', 'DROP', 'LOCK TABLES', 'INDEX']
